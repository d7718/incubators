package com.example.amazonclone.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.amazonclone.data.model.ProductInformation;
import com.example.amazonclone.service.CommonService;

@RestController
@RequestMapping(value = "/ecommerce")
public class CommonController {

    @Autowired
    private CommonService commonService;

    @PostMapping(value = "/search-by-name")
    public ResponseEntity<List<ProductInformation>> searchByName(@RequestBody Map<String, String> entity) {
        // Check Data

        return commonService.searchByName(entity);
    }

    @PostMapping(value = "/show-product")
    public ResponseEntity<?>/* <List<ProductInformation>> */ showProduct(@RequestBody Map entity) {
        // Check Data

        return commonService.showProduct(entity);
    }

    @PostMapping(value = "/get-product")
    public ResponseEntity<?>/* <List<ProductInformation>> */ getProduct(@RequestBody Map entity) {
        // Check Data

        return commonService.getProduct(entity);
    }
}
