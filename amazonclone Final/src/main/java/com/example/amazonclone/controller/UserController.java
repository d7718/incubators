package com.example.amazonclone.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.amazonclone.data.model.ProductInformation;
import com.example.amazonclone.data.model.ViewCart;
import com.example.amazonclone.service.UserService;

@RestController
@RequestMapping(value = "/ecommerce")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/sign-up")
    public ResponseEntity<String> signUp(@RequestBody Map<String, String> entity) {
        // // Check Data
        // if (entity == null) {
        // return ResponseEntity.badRequest().body("Provide Body");
        // }

        // if (entity.size() == 0 || entity.get("name") == null &&
        // entity.get("phonenumber") == null
        // && entity.get("question") == null && entity.get("answer") == null &&
        // entity.get("password") == null) {
        // return ResponseEntity.badRequest().body(" Provide Required Data ");
        // }

        // if (entity.get("name") == null || entity.get("name").isEmpty() ||
        // entity.get("name").isBlank()) {
        // return ResponseEntity.badRequest().body("Provide name");
        // }

        // if (entity.get("phonenumber") == null || entity.get("phonenumber").isEmpty()
        // || entity.get("phonenumber").isBlank()) {
        // return ResponseEntity.badRequest().body("Provide phone number");
        // }

        // if (entity.get("question") == null || entity.get("question").isEmpty() ||
        // entity.get("question").isBlank()) {
        // return ResponseEntity.badRequest().body("select question");
        // }

        // if (entity.get("answer") == null || entity.get("answer").isEmpty() ||
        // entity.get("answer").isBlank()) {
        // return ResponseEntity.badRequest().body("Provide answer");
        // }

        // if (entity.get("password") == null || entity.get("password").isEmpty() ||
        // entity.get("password").isBlank()) {
        // return ResponseEntity.badRequest().body("Provide password");
        // }

        return userService.signUp(entity);

    }

    @PostMapping("/log-in")
    public ResponseEntity<Map<String, String>> logIn(@RequestBody Map<String, String> entity) {
        return userService.logIn(entity);
    }

    @PostMapping("/log-out")
    public ResponseEntity<String> logOut(@RequestBody Map<String, String> entity) {
        // Check Data

        return userService.logOut(entity);
    }

    @PostMapping("/get-question")
    public ResponseEntity<String> getQuestion(@RequestBody Map<String, String> entity) {
        // Check Data

        return userService.getQuestion(entity);
    }

    @PostMapping("/update-password")
    public ResponseEntity<String> updatePassword(@RequestBody Map<String, String> entity) {
        // Check Data

        return userService.updatePassword(entity);
    }

    @PostMapping("/add-to-wishlist")
    public ResponseEntity<String> addToWishList(@RequestBody Map entity) {
        // Check Data

        return userService.addToWishList(entity);
    }

    @PostMapping("/show-wishlist")
    public ResponseEntity<?> showWishList(@RequestBody Map entity) {
        // Check Data

        return userService.showWishList(entity);
    }

    @PostMapping("/remove-from-wishlist")
    public ResponseEntity<String> removeFromWishList(@RequestBody Map entity) {
        // Check Data

        return userService.removeFromWishList(entity);
    }

    @PostMapping("/add-to-cart")
    public ResponseEntity<String> addToCart(@RequestBody Map entity) {
        // Check Data

        return userService.addToCart(entity);
    }

    @PostMapping("/show-cart")
    public ResponseEntity<?> showCart(@RequestBody Map entity) {
        // Check Data

        return userService.showCart(entity);
    }

    @PostMapping("/remove-from-cart")
    public ResponseEntity<String> removeFromCart(@RequestBody Map entity) {
        // Check Data

        return userService.removeFromCart(entity);
    }
}
