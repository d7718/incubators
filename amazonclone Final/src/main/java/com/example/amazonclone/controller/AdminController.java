package com.example.amazonclone.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.amazonclone.service.AdminService;

@RestController
@RequestMapping("/ecommerce")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping(value = "/add-product")
    public ResponseEntity<String> addProduct(@RequestBody Map entity) {
        // Check Data

        return adminService.addProduct(entity);
    }

    @PostMapping(value = "/edit-product")
    public ResponseEntity<String> editProduct(@RequestBody Map entity) {
        // Check Data

        return adminService.editProduct(entity);
    }

    @PostMapping(value = "/delete-product")
    public ResponseEntity<String> deleteProduct(@RequestBody Map<String, String> entity) {
        // Check Data

        return adminService.deleteProduct(entity);
    }

}
