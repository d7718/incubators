package com.example.amazonclone.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.amazonclone.data.model.UserInformation;

public interface UserInformationRepository extends JpaRepository<UserInformation, String>{
    
}
