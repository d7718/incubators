package com.example.amazonclone.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.amazonclone.data.model.ProductInformation;

public interface ProductInformationRepository extends JpaRepository<ProductInformation, Integer> {

    public Optional<ProductInformation> findByProductname(String productname);

    public Page<ProductInformation> findAllByCategory(Pageable pageable, String category);

    public List<ProductInformation> findAllByCategory(String category);

    @Query(value = "SELECT* FROM product_info", nativeQuery = true)
    public Page<ProductInformation> findAllWithPagination(Pageable pageable);

    List<ProductInformation> findByProductnameContaining(String name);

}
