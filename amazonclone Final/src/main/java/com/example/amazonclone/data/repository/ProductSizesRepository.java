package com.example.amazonclone.data.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.amazonclone.data.model.ProductSizes;

public interface ProductSizesRepository extends JpaRepository<ProductSizes, Integer> {

    @Query(value = "DELETE FROM product_sizes where productid = :productId", nativeQuery = true)
    @Modifying
    @Transactional
    public void deleteByProductid(Integer productId);
}
