package com.example.amazonclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "product_info")
public class ProductInformation {

    public ProductInformation() {
    }

    public ProductInformation(String productname, String description, String category, String price) {
        this.productname = productname;
        this.description = description;
        this.category = category;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private Integer id;

    @Column(name = "productname", unique = true, updatable = true, nullable = false)
    private String productname;

    @Column(name = "description", unique = false, updatable = true, nullable = false)
    private String description;

    @Column(name = "category", unique = false, updatable = true, nullable = false)
    private String category;

    @Column(name = "price", unique = false, updatable = true, nullable = false)
    private String price;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productInformation")
    private List<ProductImages> productImages = new ArrayList<ProductImages>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productInformation")
    private List<ProductSizes> productsizes = new ArrayList<ProductSizes>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productInformation")
    private List<ProductColors> productcolors = new ArrayList<ProductColors>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<ProductImages> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImages> productImages) {
        this.productImages = productImages;
    }

    public List<ProductSizes> getProductsizes() {
        return productsizes;
    }

    public void setProductsizes(List<ProductSizes> productsizes) {
        this.productsizes = productsizes;
    }

    public List<ProductColors> getProductcolors() {
        return productcolors;
    }

    public void setProductcolors(List<ProductColors> productcolors) {
        this.productcolors = productcolors;
    }

}
