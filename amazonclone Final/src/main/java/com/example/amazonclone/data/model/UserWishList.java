package com.example.amazonclone.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "user_wishlist")
public class UserWishList {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "userid")
    private UserInformation userInformation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wishid", unique = true, updatable = false, nullable = false)
    private Integer wishid;

    @Column(name = "productid", unique = false, updatable = true, nullable = false)
    private Integer productid;

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public Integer getWishid() {
        return wishid;
    }

    public void setWishid(Integer wishid) {
        this.wishid = wishid;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    
}
