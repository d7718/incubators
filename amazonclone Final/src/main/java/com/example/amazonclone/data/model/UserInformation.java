package com.example.amazonclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "user_info")
public class UserInformation {

    @Id
    @Column(name = "phonenumber", unique = true, updatable = false, nullable = false)
    private String phonenumber;

    @Column(name = "name", unique = false, updatable = true, nullable = false)
    private String name;

    @Column(name = "question", unique = false, updatable = false, nullable = false)
    private String question;

    @Column(name = "answer", unique = false, updatable = false, nullable = false)
    private String answer;

    @Column(name = "password", unique = false, updatable = true, nullable = false)
    private String password;

    @Column(name = "admin", unique = false, updatable = false, nullable = false)
    private Integer admin = 0;

    @Column(name = "loginstatus", unique = false, updatable = true, nullable = true)
    private Integer loginStatus;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userInformation")
    private List<UserWishList> userWishlist = new ArrayList<UserWishList>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userInformation")
    private List<CartProductInformation> cartProductInformations = new ArrayList<CartProductInformation>();

    public UserInformation() {

    }

    public UserInformation(String name, String phonenumber, String question, String answer, String password,
            Integer loginStatus) {

        this.name = name;
        this.phonenumber = phonenumber;
        this.question = question;
        this.answer = answer;
        this.password = password;
        this.loginStatus = loginStatus;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAdmin() {
        return admin;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    public List<UserWishList> getUserWishlist() {
        return userWishlist;
    }

    public void setUserWishlist(List<UserWishList> userWishlist) {
        this.userWishlist = userWishlist;
    }

    public List<CartProductInformation> getCartProductInformations() {
        return cartProductInformations;
    }

    public void setCartProductInformations(List<CartProductInformation> cartProductInformations) {
        this.cartProductInformations = cartProductInformations;
    }

}