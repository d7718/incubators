package com.example.amazonclone.data.model;

public class ViewCart {

    private String selectedcolor;
    private String selectedsize;
    private Integer quantity;

    private ProductInformation productInformation;

    public String getSelectedcolor() {
        return selectedcolor;
    }

    public void setSelectedcolor(String selectedcolor) {
        this.selectedcolor = selectedcolor;
    }

    public String getSelectedsize() {
        return selectedsize;
    }

    public void setSelectedsize(String selectedsize) {
        this.selectedsize = selectedsize;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ProductInformation getProductInformation() {
        return productInformation;
    }

    public void setProductInformation(ProductInformation productInformation) {
        this.productInformation = productInformation;
    }
}
