package com.example.amazonclone.data.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.amazonclone.data.model.UserWishList;

public interface UserWishListRepository extends JpaRepository<UserWishList, Integer> {

    @Query(value = "SELECT* FROM user_wishlist wish WHERE wish.userid = :userId AND wish.productid = :productId", nativeQuery = true)
    public Optional<UserWishList> findByUseridAndProductid(String userId, Integer productId);

    @Query(value = "SELECT* FROM user_wishlist wish WHERE wish.userid = :userId", nativeQuery = true)
    public ArrayList<UserWishList> findAByUserid(String userId);
}
