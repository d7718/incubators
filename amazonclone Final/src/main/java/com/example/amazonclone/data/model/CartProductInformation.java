package com.example.amazonclone.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "cart_product_info")
public class CartProductInformation {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "userid")
    private UserInformation userInformation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cartid", unique = true, updatable = false, nullable = false)
    private Integer cartid;

    @Column(name = "productid", unique = false, updatable = true, nullable = false)
    private Integer productid;

    @Column(name = "selectedsize", unique = false, updatable = true, nullable = false)
    private String selectedsize;

    @Column(name = "selectedcolor", unique = false, updatable = true, nullable = false)
    private String selectedcolor;

    @Column(name = "quantity", unique = false, updatable = true, nullable = false)
    private Integer quantity;

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public Integer getCartid() {
        return cartid;
    }

    public void setCartid(Integer cartid) {
        this.cartid = cartid;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public String getSelectedsize() {
        return selectedsize;
    }

    public void setSelectedsize(String selectedsize) {
        this.selectedsize = selectedsize;
    }

    public String getSelectedcolor() {
        return selectedcolor;
    }

    public void setSelectedcolor(String selectedcolor) {
        this.selectedcolor = selectedcolor;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
