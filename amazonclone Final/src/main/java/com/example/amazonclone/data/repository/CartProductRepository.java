package com.example.amazonclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.amazonclone.data.model.CartProductInformation;

public interface CartProductRepository extends JpaRepository<CartProductInformation, Integer> {

    @Query(value = "SELECT* FROM cart_product_info prod WHERE prod.userid = :userId AND prod.productid = :productId AND prod.selectedcolor = :color AND prod.selectedsize = :size", nativeQuery = true)
    public Optional<CartProductInformation> findByUseridAndProductidAndSelectedcolorAndSelectedsize(String userId,
            Integer productId, String color, String size);
}
