package com.example.amazonclone.entryPoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.example.amazonclone.data.repository")
@EntityScan("com.example.amazonclone.data.model")
@ComponentScan(basePackages = { "com.example.amazonclone.controller", "com.example.amazonclone.service",
		"com.example.amazonclone.filter" })

@SpringBootApplication
public class AmazoncloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmazoncloneApplication.class, args);
	}

}
