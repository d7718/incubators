package com.example.amazonclone.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.amazonclone.data.model.ProductInformation;
import com.example.amazonclone.data.repository.ProductInformationRepository;

@Service
public class CommonService {

    @Autowired
    private ProductInformationRepository productInformationRepository;

    public ResponseEntity<List<ProductInformation>> searchByName(Map<String, String> entity) {

        String search = entity.get("search");
        String[] words = search.split(" ");

        List<ProductInformation> common = new ArrayList<ProductInformation>();
        // ListIterator<ProductInformation> ll = product.listIterator();

        for (int i = 0; i < words.length; i++) {

            if (words[i] == "") {
                continue;
            }
            common.addAll(productInformationRepository.findByProductnameContaining(words[i]));
            // ListIterator<ProductInformation> lcheck = common.listIterator();
        }

        Set<ProductInformation> set = new LinkedHashSet<ProductInformation>(common);
        List<ProductInformation> product = new ArrayList<ProductInformation>(set);
        // while (lcheck.hasNext()) {

        // ProductInformation commonCheck = lcheck.next();

        // while (ll.hasNext()) {

        // ProductInformation check = ll.next();
        // if (check.getId() == commonCheck.getId()) {
        // flag = 1;
        // break;
        // }
        // }
        // if (flag == 0) {
        // product.add(commonCheck);
        // }
        // }

        // Optional<ProductInformation> prodct = productInformationRepository
        // .findByProductname((String) entity.get("productname"));

        // if (prodct.isPresent()) {
        // return ResponseEntity.badRequest().body("Product already exist!");
        // }

        return ResponseEntity.ok().body(product);
    }

    public ResponseEntity<?>/* <List<ProductInformation>> */ showProduct(Map entity) {

        List<ProductInformation> response;
        // Pageable pageable = PageRequest.of((int) entity.get("page"), 20);

        // Page<ProductInformation> page;

        if (entity.containsKey("category")) {
            // page = productInformationRepository.findAllByCategory(pageable,(String)
            // entity.get("category"));

            response = productInformationRepository.findAllByCategory((String) entity.get("category"));

            // if (page.hasContent())
            // response = page.getContent();
            // else
            // return ResponseEntity.ok().body(null);
        } else {
            response = productInformationRepository.findAll();
            // page = productInformationRepository.findAllWithPagination(pageable);
            // if (page.hasContent())
            // response = page.getContent();
            // else
            // return ResponseEntity.ok().body(null);
        }

        return ResponseEntity.ok().body(response);

    }

    public ResponseEntity<?> getProduct(Map entity) {

        Optional<ProductInformation> product = productInformationRepository.findById((Integer) entity.get("productid"));

        if (!product.isPresent())
            return ResponseEntity.ok().body("Product does not exist");

        ProductInformation productObj = product.get();

        return ResponseEntity.ok().body(productObj);
    }
}
