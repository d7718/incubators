package com.example.amazonclone.service;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.amazonclone.data.model.ProductColors;
import com.example.amazonclone.data.model.ProductImages;
import com.example.amazonclone.data.model.ProductInformation;
import com.example.amazonclone.data.model.ProductSizes;
import com.example.amazonclone.data.repository.ProductColorsRepository;
import com.example.amazonclone.data.repository.ProductImagesRepository;
import com.example.amazonclone.data.repository.ProductInformationRepository;
import com.example.amazonclone.data.repository.ProductSizesRepository;

@Service
public class AdminService {

    @Autowired
    private ProductImagesRepository productImagesRepository;

    @Autowired
    private ProductInformationRepository productInformationRepository;

    @Autowired
    private ProductColorsRepository productColorsRepository;

    @Autowired
    private ProductSizesRepository productSizesRepository;

    public ResponseEntity<String> addProduct(Map entity) {

        Optional<ProductInformation> prodct = productInformationRepository
                .findByProductname((String) entity.get("productname"));

        if (prodct.isPresent()) {
            return ResponseEntity.ok().body("Product already exist!");
        }

        ProductInformation product = new ProductInformation((String) entity.get("productname"),
                (String) entity.get("description"),
                (String) entity.get("category"), (String) entity.get("price"));
        // ................................................................
        ArrayList<String> productLinks = (ArrayList) entity.get("link");
        ListIterator<String> ll = productLinks.listIterator();

        while (ll.hasNext()) {

            ProductImages img = new ProductImages();
            String link = ll.next();
            img.setLink(link);
            img.setProductInformation(product);
            product.getProductImages().add(img);
            productInformationRepository.save(product);
        }

        ArrayList<String> productSizes = (ArrayList) entity.get("size");
        ll = productSizes.listIterator();

        while (ll.hasNext()) {

            ProductSizes sizes = new ProductSizes();
            String size = ll.next();
            sizes.setSize(size);
            sizes.setProductInformation(product);
            product.getProductsizes().add(sizes);
            productInformationRepository.save(product);
        }

        ArrayList<String> productColors = (ArrayList) entity.get("color");
        ll = productColors.listIterator();

        while (ll.hasNext()) {

            ProductColors colors = new ProductColors();
            String color = ll.next();
            colors.setColor(color);
            colors.setProductInformation(product);
            product.getProductcolors().add(colors);
            productInformationRepository.save(product);
        }
        // ...........................................

        return ResponseEntity.ok().body("Product added successfully.");
    }

    public ResponseEntity<String> editProduct(Map entity) {

        // Assuming the id will be integer value.
        Optional<ProductInformation> exproduct = productInformationRepository
                .findById((Integer) entity.get("productid"));

        if (!exproduct.isPresent()) {

            return ResponseEntity.ok().body("Product Does Not Exists!");
        }

        ProductInformation product = exproduct.get();

        product.setProductname((String) entity.get("productname"));
        product.setDescription((String) entity.get("description"));
        product.setCategory((String) entity.get("category"));
        product.setPrice((String) entity.get("price"));

        ArrayList<String> productLinks = (ArrayList) entity.get("link");
        ListIterator<String> ll = productLinks.listIterator();

        productImagesRepository.deleteByProductid(product.getId());

        while (ll.hasNext()) {

            ProductImages img = new ProductImages();
            String link = ll.next();
            img.setLink(link);
            img.setProductInformation(product);
            product.getProductImages().add(img);
            productInformationRepository.save(product);
        }

        ArrayList<String> productSizes = (ArrayList) entity.get("size");
        ll = productSizes.listIterator();

        productSizesRepository.deleteByProductid(product.getId());

        while (ll.hasNext()) {

            ProductSizes sizes = new ProductSizes();
            String size = ll.next();
            sizes.setSize(size);
            sizes.setProductInformation(product);
            product.getProductsizes().add(sizes);
            productInformationRepository.save(product);
        }

        ArrayList<String> productColors = (ArrayList) entity.get("color");
        ll = productColors.listIterator();

        productColorsRepository.deleteByProductid(product.getId());

        while (ll.hasNext()) {

            ProductColors colors = new ProductColors();
            String color = ll.next();
            colors.setColor(color);
            colors.setProductInformation(product);
            product.getProductcolors().add(colors);
            productInformationRepository.save(product);
        }

        return ResponseEntity.ok().body("Product Information Updated Successfully!");

    }

    public ResponseEntity<String> deleteProduct(Map<String, String> entity) {

        Integer id;

        try {
            id = Integer.parseInt(entity.get("productid"));
        } catch (NumberFormatException numberFormatException) {
            return ResponseEntity.ok().body("ID must be an integer value.");

        }
        Optional<ProductInformation> exproduct = productInformationRepository.findById(id);

        if (!exproduct.isPresent()) {

            return ResponseEntity.ok().body("Product Not Exists!");
        }

        productInformationRepository.deleteById(id);

        return ResponseEntity.ok().body("Product deleted successfully!");

    }
}
