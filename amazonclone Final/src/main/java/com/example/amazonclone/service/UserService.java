package com.example.amazonclone.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.amazonclone.data.model.CartProductInformation;
import com.example.amazonclone.data.model.ProductInformation;
import com.example.amazonclone.data.model.UserInformation;
import com.example.amazonclone.data.model.UserWishList;
import com.example.amazonclone.data.model.ViewCart;
import com.example.amazonclone.data.repository.CartProductRepository;
import com.example.amazonclone.data.repository.ProductInformationRepository;
import com.example.amazonclone.data.repository.UserInformationRepository;
import com.example.amazonclone.data.repository.UserWishListRepository;

@Service
public class UserService {

    @Autowired
    private UserInformationRepository userInformationRepository;

    @Autowired
    private UserWishListRepository userWishListRepository;

    @Autowired
    private ProductInformationRepository productInformationRepository;

    @Autowired
    private CartProductRepository cartProductRepository;

    public ResponseEntity<String> addToCartentity;

    public ResponseEntity<String> signUp(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findById(entity.get("phonenumber"));

        if (user.isPresent()) {
            return ResponseEntity.ok().body("User already registered");
        }
        UserInformation userInformation = new UserInformation(entity.get("name"), entity.get("phonenumber"),
                entity.get("question"), entity.get("answer"), entity.get("password"), 1);

        userInformationRepository.save(userInformation);

        return ResponseEntity.ok().body("You are signed up successfully");
    }

    public ResponseEntity<Map<String, String>> logIn(Map<String, String> entity) {
        int flag = 0;

        Optional<UserInformation> user = userInformationRepository.findById(entity.get("phonenumber"));
        Map<String, String> response = new HashMap<String, String>();

        if (!user.isPresent()) {
            response.put("message", "User already registered");
            flag = 1;

        }

        UserInformation userObj = user.get();

        if (!userObj.getPassword().equals(entity.get("password")) && flag == 0) {
            response.put("message", "Invalid password");
            flag = 1;
        }

        if (userObj.getLoginStatus() == 1 && flag == 0) {
            response.put("message", "Already Logged in");
            flag = 1;
        }

        if (flag == 0) {
            userObj.setLoginStatus(1);
            response.put("message", "Logged in successfully");
            response.put("admin", userObj.getAdmin().toString());
            userInformationRepository.save(userObj);

        }

        return ResponseEntity.ok().body(response);

    }

    // public ResponseEntity<String> forgotPassword(Map<String, String> entity) {

    // Optional<UserInformation> user =
    // userInformationRepository.findById(entity.get("phonenumber"));

    // if (!user.isPresent()) {
    // return ResponseEntity.badRequest().body("User does not exist.");
    // }

    // UserInformation userObj = user.get();

    // if (!userObj.getQuestion().equals(entity.get("question")))
    // return ResponseEntity.badRequest().body("Please select correct question.");

    // if (!userObj.getAnswer().equals(entity.get("answer")))
    // return ResponseEntity.badRequest().body("Wrong answer.");

    // userObj.setAnswer(entity.get("password"));
    // userInformationRepository.save(userObj);

    // return ResponseEntity.ok().body("Password updated successfully");

    // }

    public ResponseEntity<String> getQuestion(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findById(entity.get("phonenumber"));

        if (!user.isPresent()) {
            return ResponseEntity.ok().body("User does not exist.");
        }

        UserInformation us = user.get();

        return ResponseEntity.ok().body(us.getQuestion());

    }

    public ResponseEntity<String> updatePassword(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findById(entity.get("phonenumber"));
        UserInformation usObj = user.get();

        if (!usObj.getAnswer().equalsIgnoreCase(entity.get("answer"))) {

            return ResponseEntity.ok().body("Incorrect answer");

            // return ResponseEntity.badRequest().body("Incorrect Answer");

        }

        if (usObj.getPassword().equals(entity.get("password"))) {

            return ResponseEntity.ok().body("Password already used");
        }

        usObj.setPassword(entity.get("password"));

        userInformationRepository.save(usObj);

        return ResponseEntity.ok().body("Password Updated Successfully");
    }

    public ResponseEntity<String> addToWishList(Map entity) {

        Optional<UserWishList> check = userWishListRepository.findByUseridAndProductid(
                (String) entity.get("phonenumber"),
                (Integer) entity.get("productid"));

        if (check.isPresent())
            return ResponseEntity.ok().body("Already in wishlist.");

        UserInformation user = userInformationRepository.findById((String) entity.get("phonenumber")).get();

        UserWishList wishlist = new UserWishList();
        wishlist.setProductid((Integer) entity.get("productid"));
        wishlist.setUserInformation(user);
        user.getUserWishlist().add(wishlist);
        userInformationRepository.save(user);

        return ResponseEntity.ok().body("Added to wishlist");
    }

    public ResponseEntity<?> showWishList(Map entity) {

        List<UserWishList> wishlist = userWishListRepository.findAByUserid((String) entity.get("phonenumber"));

        List<ProductInformation> productList = new ArrayList<ProductInformation>();

        if (wishlist.isEmpty())
            return ResponseEntity.ok().body("Wishlist is Empty!");

        ListIterator<UserWishList> ll = wishlist.listIterator();

        while (ll.hasNext()) {
            Integer productid = ll.next().getProductid();

            Optional<ProductInformation> productCheck = productInformationRepository.findById(productid);

            if (!productCheck.isPresent())
                continue;

            productList.add(productCheck.get());
        }

        return ResponseEntity.ok().body(productList);
    }

    public ResponseEntity<String> removeFromWishList(Map entity) {

        Optional<UserWishList> remove = userWishListRepository
                .findByUseridAndProductid((String) entity.get("phonenumber"), (Integer) entity.get("productid"));

        userWishListRepository.delete(remove.get());

        return ResponseEntity.ok().body("Removed from wishlist");
    }

    public ResponseEntity<String> addToCart(Map entity) {

        Optional<CartProductInformation> check = cartProductRepository
                .findByUseridAndProductidAndSelectedcolorAndSelectedsize((String) entity.get("phonenumber"),
                        (Integer) entity.get("productid"), (String) entity.get("selectedcolor"),
                        (String) entity.get("selectedsize"));

        UserInformation user = userInformationRepository.findById((String) entity.get("phonenumber")).get();

        if (check.isPresent()) {

            CartProductInformation cartProd = check.get();

            cartProd.setQuantity(cartProd.getQuantity() + (Integer) entity.get("quantity"));
            cartProd.setUserInformation(user);
            user.getCartProductInformations().add(cartProd);
            userInformationRepository.save(user);

            return ResponseEntity.ok().body("Added to cart.");
        }

        CartProductInformation cartProd = new CartProductInformation();
        cartProd.setProductid((Integer) entity.get("productid"));
        cartProd.setQuantity((Integer) entity.get("quantity"));
        cartProd.setSelectedcolor((String) entity.get("selectedcolor"));
        cartProd.setSelectedsize((String) entity.get("selectedsize"));

        cartProd.setUserInformation(user);
        user.getCartProductInformations().add(cartProd);
        userInformationRepository.save(user);

        return ResponseEntity.ok().body("Added to cart.");

    }

    public ResponseEntity<?> showCart(Map entity) {

        UserInformation user = userInformationRepository.findById((String) entity.get("phonenumber")).get();
        List<CartProductInformation> cart = user.getCartProductInformations();
        ListIterator<CartProductInformation> ll = cart.listIterator();

        List<ViewCart> response = new ArrayList<ViewCart>();

        while (ll.hasNext()) {

            CartProductInformation cartProd = ll.next();
            ProductInformation product = productInformationRepository.findById((Integer) cartProd.getProductid()).get();

            ViewCart viewCart = new ViewCart();
            viewCart.setProductInformation(product);
            viewCart.setQuantity(cartProd.getQuantity());
            viewCart.setSelectedcolor(cartProd.getSelectedcolor());
            viewCart.setSelectedsize(cartProd.getSelectedsize());

            response.add(viewCart);
        }

        if (response.isEmpty())
            return ResponseEntity.ok().body("Cart is Empty!");

        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<String> removeFromCart(Map entity) {

        Optional<CartProductInformation> remove = cartProductRepository
                .findByUseridAndProductidAndSelectedcolorAndSelectedsize((String) entity.get("phonenumber"),
                        (Integer) entity.get("productid"), (String) entity.get("selectedcolor"),
                        (String) entity.get("selectedsize"));

        cartProductRepository.delete(remove.get());

        return ResponseEntity.ok().body("Removed from cart");
    }

    public ResponseEntity<String> logOut(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findById(entity.get("phonenumber"));
        UserInformation userObj = user.get();

        userObj.setLoginStatus(0);
        userInformationRepository.save(userObj);

        return ResponseEntity.ok().body("Logged Out Successfully");

    }

}
