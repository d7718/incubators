package com.example.netflixclone.dto.UserDto;

import javax.validation.constraints.NotNull;

public class AddToWatchingListRequest {

    @NotNull(message = "Provide user id for backend purpose.")
    private Integer userid;

    @NotNull(message = "Provide watching id.")
    private Integer watching;

    @NotNull(message = "Provide flag series for backend purpose.")
    private Integer series;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getWatching() {
        return watching;
    }

    public void setWatching(Integer watching) {
        this.watching = watching;
    }

    public Integer getSeries() {
        return series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

}
