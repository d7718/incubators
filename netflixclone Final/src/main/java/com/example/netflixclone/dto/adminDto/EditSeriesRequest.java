package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotNull;

public class EditSeriesRequest extends AddSeriesRequest {

    @NotNull(message = "Provide series id for backend purpose")
    private Integer seriesid;

    public Integer getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(Integer seriesid) {
        this.seriesid = seriesid;
    }

}
