package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddSeasonRequest {

    @NotNull(message = "Provide series id for backend purpose")
    private Integer seriesid;

    @NotBlank(message = "Season name is required.")
    private String seasonnum;

    public Integer getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(Integer seriesid) {
        this.seriesid = seriesid;
    }

    public String getSeasonnum() {
        return seasonnum;
    }

    public void setSeasonnum(String seasonnum) {
        this.seasonnum = seasonnum;
    }

}
