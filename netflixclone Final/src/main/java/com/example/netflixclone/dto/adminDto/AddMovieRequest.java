package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotBlank;

public class AddMovieRequest {

    @NotBlank(message = "Movie name is required.")
    private String moviename;

    @NotBlank(message = "Description is required.")
    private String description;

    @NotBlank(message = "Category is required.")
    private String category;

    @NotBlank(message = "Trailer is required.")
    private String trailer;

    @NotBlank(message = "Poster is required.")
    private String poster;

    @NotBlank(message = "Movie is required.")
    private String movie;

    public String getMoviename() {
        return moviename;
    }

    public void setMoviename(String moviename) {
        this.moviename = moviename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

}
