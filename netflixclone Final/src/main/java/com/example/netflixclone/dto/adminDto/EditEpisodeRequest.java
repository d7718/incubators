package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotNull;

public class EditEpisodeRequest extends AddEpisodeRequest {

    @NotNull(message = "Provide episode id for backend purpose.")
    private Integer episodeid;

    public Integer getEpisodeid() {
        return episodeid;
    }

    public void setEpisodeid(Integer episodeid) {
        this.episodeid = episodeid;
    }

}
