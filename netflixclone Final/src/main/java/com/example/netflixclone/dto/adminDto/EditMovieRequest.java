package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotNull;

public class EditMovieRequest extends AddMovieRequest {

    @NotNull(message = "Provide movie id for backend purpose")
    private Integer movieid;

    public Integer getMovieid() {
        return movieid;
    }

    public void setMovieid(Integer movieid) {
        this.movieid = movieid;
    }

}
