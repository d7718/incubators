package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddEpisodeRequest {

    @NotNull(message = "Provide season id for backend purpose.")
    private Integer seasonid;

    @NotBlank(message = "Episode number is required.")
    private String episodenum;

    @NotBlank(message = "Episode name is required.")
    private String episodename;

    @NotBlank(message = "Description is required.")
    private String description;

    @NotBlank(message = "Poster is required.")
    private String poster;

    @NotBlank(message = "Episode is required.")
    private String episode;

    public Integer getSeasonid() {
        return seasonid;
    }

    public void setSeasonid(Integer seasonid) {
        this.seasonid = seasonid;
    }

    public String getEpisodenum() {
        return episodenum;
    }

    public void setEpisodenum(String episodenum) {
        this.episodenum = episodenum;
    }

    public String getEpisodename() {
        return episodename;
    }

    public void setEpisodename(String episodename) {
        this.episodename = episodename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

}
