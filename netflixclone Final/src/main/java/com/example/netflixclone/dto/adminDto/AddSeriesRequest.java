package com.example.netflixclone.dto.adminDto;

import javax.validation.constraints.NotBlank;

public class AddSeriesRequest {

    @NotBlank(message = "Series name is required.")
    private String seriesname;

    @NotBlank(message = "Description is required.")
    private String description;

    @NotBlank(message = "Category is required.")
    private String category;

    @NotBlank(message = "Trailer is required.")
    private String trailer;

    @NotBlank(message = "Poster is required.")
    private String poster;

    public String getSeriesname() {
        return seriesname;
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

}
