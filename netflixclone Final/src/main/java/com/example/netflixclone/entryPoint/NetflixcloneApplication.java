package com.example.netflixclone.entryPoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.example.netflixclone.data.repository")
@EntityScan("com.example.netflixclone.data.model")
@ComponentScan(basePackages = { "com.example.netflixclone.controller", "com.example.netflixclone.service",
		"com.example.netflixclone.filter" })
@SpringBootApplication
public class NetflixcloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(NetflixcloneApplication.class, args);
	}

}
