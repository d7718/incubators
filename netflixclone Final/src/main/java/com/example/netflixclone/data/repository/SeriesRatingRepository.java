package com.example.netflixclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.SeriesRating;

public interface SeriesRatingRepository extends JpaRepository<SeriesRating, Integer> {

    @Query(value = "SELECT* FROM series_rating erating WHERE erating.userid = :userid AND mrating.seriesid = :seriesid", nativeQuery = true)
    Optional<SeriesRating> findByUseridAndSeriesid(Integer userid, Integer seriesid);
}
