package com.example.netflixclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.EpisodeInformation;

public interface EpisodeInformationRepository extends JpaRepository<EpisodeInformation, Integer> {

    @Query(value = "SELECT* FROM episode_info epi WHERE epi.seasonid = :seasonid AND epi.episodenum = :episodenum", nativeQuery = true)
    public Optional<EpisodeInformation> findBySeasonidAndEpisodenum(Integer seasonid, Integer episodenum);

    @Query(value = "SELECT* FROM episode_info epi WHERE epi.seasonid = :seasonid AND epi.episodename = :episodename", nativeQuery = true)
    public Optional<EpisodeInformation> findBySeasonidAndEpisodename(Integer seasonid, String episodename);

}
