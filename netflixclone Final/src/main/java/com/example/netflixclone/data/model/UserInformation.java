package com.example.netflixclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "user_info")
public class UserInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private Integer id;

    @Column(name = "phonenumber", unique = true, updatable = true, nullable = false)
    private String phonenumber;

    @Column(name = "name", unique = false, updatable = true, nullable = false)
    private String name;

    @Column(name = "question", unique = false, updatable = false, nullable = false)
    private String question;

    @Column(name = "answer", unique = false, updatable = false, nullable = false)
    private String answer;

    @Column(name = "password", unique = false, updatable = true, nullable = false)
    private String password;

    @Column(name = "email", unique = false, updatable = true, nullable = false)
    private String email;

    @Column(name = "admin", unique = false, updatable = false, nullable = false)
    private Integer admin = 0;

    @Column(name = "loginstatus", unique = false, updatable = true, nullable = true)
    private Integer loginStatus;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userInformation")
    private List<WatchingInformation> watchingList = new ArrayList<WatchingInformation>();

    // @JsonManagedReference
    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "userInformation")
    // private List<CartProductInformation> cartProductInformations = new
    // ArrayList<CartProductInformation>();

    public UserInformation() {

    }

    public UserInformation(String name, String phonenumber, String question, String answer, String password,
            String email, Integer loginStatus) {

        this.name = name;
        this.email = email;
        this.phonenumber = phonenumber;
        this.question = question;
        this.answer = answer;
        this.password = password;
        this.loginStatus = loginStatus;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAdmin() {
        return admin;
    }

    public void setAdmin(Integer admin) {
        this.admin = admin;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    public List<WatchingInformation> getWatchingList() {
        return watchingList;
    }

    public void setWatchingList(List<WatchingInformation> watchingList) {
        this.watchingList = watchingList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
}