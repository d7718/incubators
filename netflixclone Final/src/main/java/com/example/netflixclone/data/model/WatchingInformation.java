package com.example.netflixclone.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "watching_info")
public class WatchingInformation {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "userid")
    private UserInformation userInformation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "watchid", unique = true, updatable = false, nullable = false)
    private Integer watchid;

    @Column(name = "watching", unique = false, updatable = false, nullable = false)
    private Integer watching;

    @Column(name = "series", unique = false, updatable = false, nullable = false)
    private Integer series;

    public WatchingInformation(){

    }

    public WatchingInformation(Integer watching, Integer series) {
        this.watching = watching;
        this.series = series;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public Integer getWatching() {
        return watching;
    }

    public void setWatching(Integer watching) {
        this.watching = watching;
    }

    public Integer getSeries() {
        return series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public Integer getWatchid() {
        return watchid;
    }

    public void setWatchid(Integer watchid) {
        this.watchid = watchid;
    }

}
