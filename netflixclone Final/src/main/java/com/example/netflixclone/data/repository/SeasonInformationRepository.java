package com.example.netflixclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.SeasonInformation;

public interface SeasonInformationRepository extends JpaRepository<SeasonInformation, Integer> {

    @Query(value = "SELECT* FROM season_info seri WHERE seri.seriesid = :seriesid AND seri.seasonnum = :seasonnum", nativeQuery = true)
    public Optional<SeasonInformation> findBySeriesidAndSeasonnum(Integer seriesid, String seasonnum);

    @Query(value = "SELECT* FROM season_info seri WHERE seri.seriesid = :seriesid AND seri.seasonid = :seasonid", nativeQuery = true)
    public Optional<SeasonInformation> findBySeriesidAndSeasonid(Integer seriesid, Integer seasonid);

}
