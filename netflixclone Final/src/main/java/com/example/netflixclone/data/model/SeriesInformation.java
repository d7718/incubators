package com.example.netflixclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "series_info")
public class SeriesInformation {

    public SeriesInformation() {
    }

    public SeriesInformation(String seriesname, String description, String category, Integer rating, String trailer,
            String poster) {
        this.seriesname = seriesname;
        this.description = description;
        this.category = category;
        this.rating = rating;
        this.poster = poster;
        this.trailer = trailer;
    }

    public SeriesInformation(String seriesname, String description, String category, String trailer,
            String poster) {
        this.seriesname = seriesname;
        this.description = description;
        this.category = category;
        rating = 0;
        this.poster = poster;
        this.trailer = trailer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private Integer id;

    @Column(name = "seriesname", unique = false, updatable = true, nullable = false)
    private String seriesname;

    @Column(name = "description", unique = false, updatable = true, nullable = false)
    private String description;

    @Column(name = "category", unique = false, updatable = true, nullable = false)
    private String category;

    @Column(name = "rating", unique = false, updatable = true, nullable = true)
    private Integer rating = 0;

    @Column(name = "poster", unique = false, updatable = true, nullable = false)
    private String poster;

    @Column(name = "trailer", unique = false, updatable = true, nullable = false)
    private String trailer;

    @Column(name = "series", unique = false, updatable = true, nullable = false)
    private Integer series = 1;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seriesInformation")
    private List<SeasonInformation> season = new ArrayList<SeasonInformation>();

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seriesInformation")
    private List<SeriesRating> ratingList = new ArrayList<SeriesRating>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public List<SeasonInformation> getSeason() {
        return season;
    }

    public void setSeason(List<SeasonInformation> season) {
        this.season = season;
    }

    public String getSeriesname() {
        return seriesname;
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public Integer getSeries() {
        return series;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public List<SeriesRating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<SeriesRating> ratingList) {
        this.ratingList = ratingList;
    }
    
}
