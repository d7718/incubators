package com.example.netflixclone.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "movie_rating")
public class MovieRating {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "movieid")
    private MovieInformation movieInformation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ratingid", unique = true, updatable = false, nullable = false)
    private Integer ratingid;

    @Column(name = "rating", unique = false, updatable = false, nullable = false)
    private Integer rating;

    @Column(name = "review", unique = false, updatable = false, nullable = true)
    private String review;

    @Column(name = "userid", unique = false, updatable = false, nullable = false)
    private Integer userid;

    @Column(name = "username", unique = false, updatable = false, nullable = false)
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public MovieInformation getMovieInformation() {
        return movieInformation;
    }

    public void setMovieInformation(MovieInformation movieInformation) {
        this.movieInformation = movieInformation;
    }

    public Integer getRatingid() {
        return ratingid;
    }

    public void setRatingid(Integer ratingid) {
        this.ratingid = ratingid;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

}
