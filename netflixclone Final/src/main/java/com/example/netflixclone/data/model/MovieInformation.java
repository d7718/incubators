package com.example.netflixclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "movie_info")
public class MovieInformation {

    public MovieInformation() {
    }

    public MovieInformation(String moviename, String description, String category, Integer rating, String trailer,
            String poster, String movie) {
        this.moviename = moviename;
        this.description = description;
        this.category = category;
        this.rating = rating;
        this.poster = poster;
        this.trailer = trailer;
        this.movie = movie;

    }

    public MovieInformation(String moviename, String description, String category, String trailer,
            String poster, String movie) {
        this.moviename = moviename;
        this.description = description;
        this.category = category;
        this.poster = poster;
        this.trailer = trailer;
        this.movie = movie;
        rating = 0;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private Integer id;

    @Column(name = "moviename", unique = false, updatable = true, nullable = false)
    private String moviename;

    @Column(name = "description", unique = false, updatable = true, nullable = false)
    private String description;

    @Column(name = "category", unique = false, updatable = true, nullable = false)
    private String category;

    @Column(name = "rating", unique = false, updatable = true, nullable = true)
    private Integer rating = 0;

    @Column(name = "poster", unique = false, updatable = true, nullable = false)
    private String poster;

    @Column(name = "trailer", unique = false, updatable = true, nullable = false)
    private String trailer;

    @Column(name = "movie", unique = false, updatable = true, nullable = false)
    private String movie;

    @Column(name = "series", unique = false, updatable = true, nullable = false)
    private Integer series = 0;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "movieInformation")
    private List<MovieRating> ratingList = new ArrayList<MovieRating>();

    public String getMoviename() {
        return moviename;
    }

    public void setMoviename(String moviename) {
        this.moviename = moviename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public Integer getSeries() {
        return series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<MovieRating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<MovieRating> ratingList) {
        this.ratingList = ratingList;
    }

}
