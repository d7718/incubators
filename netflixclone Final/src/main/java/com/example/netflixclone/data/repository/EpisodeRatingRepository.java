package com.example.netflixclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.EpisodeInformation;
import com.example.netflixclone.data.model.EpisodeRating;

public interface EpisodeRatingRepository extends JpaRepository<EpisodeInformation, Integer> {

   
    @Query(value = "SELECT* FROM episode_rating erating WHERE erating.userid = :userid AND mrating.episodeid = :episodeid", nativeQuery = true)
     Optional<EpisodeRating> findByUseridAndEpisodeid(Integer userid, Integer episodeid);

}
