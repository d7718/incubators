package com.example.netflixclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "episode_info")
public class EpisodeInformation {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "seasonid")
    private SeasonInformation seasonInformation;

    public EpisodeInformation() {

    }

    public EpisodeInformation(Integer episodenum, String episodename, String description, String poster,
            String episode) {
        this.episodenum = episodenum;
        this.episodename = episodename;
        this.description = description;
        this.poster = poster;
        this.episode = episode;
        rating = 0;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "episodeid", unique = true, updatable = false, nullable = false)
    private Integer episodeid;

    @Column(name = "episodenum", unique = false, updatable = true, nullable = false)
    private Integer episodenum;

    @Column(name = "episodename", unique = false, updatable = true, nullable = false)
    private String episodename;

    @Column(name = "description", unique = false, updatable = true, nullable = false)
    private String description;

    @Column(name = "poster", unique = false, updatable = true, nullable = false)
    private String poster;

    @Column(name = "episode", unique = false, updatable = true, nullable = false)
    private String episode;

    @Column(name = "rating", unique = false, updatable = true, nullable = true)
    private Integer rating = 0;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "episodeInformation")
    private List<EpisodeRating> ratingList = new ArrayList<EpisodeRating>();

    public Integer getEpisodenum() {
        return episodenum;
    }

    public void setEpisodenum(Integer episodenum) {
        this.episodenum = episodenum;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public SeasonInformation getSeasonInformation() {
        return seasonInformation;
    }

    public void setSeasonInformation(SeasonInformation seasonInformation) {
        this.seasonInformation = seasonInformation;
    }

    public Integer getEpisodeid() {
        return episodeid;
    }

    public void setEpisodeid(Integer episodeid) {
        this.episodeid = episodeid;
    }

    public String getEpisodename() {
        return episodename;
    }

    public void setEpisodename(String episodename) {
        this.episodename = episodename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public List<EpisodeRating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<EpisodeRating> ratingList) {
        this.ratingList = ratingList;
    }

}
