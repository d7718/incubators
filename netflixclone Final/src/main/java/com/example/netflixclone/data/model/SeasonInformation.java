package com.example.netflixclone.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "season_info")
public class SeasonInformation {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "seriesid")
    private SeriesInformation seriesInformation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seasonid", unique = true, updatable = false, nullable = false)
    private Integer seasonid;

    @Column(name = "seasonnum", unique = false, updatable = false, nullable = false)
    private String seasonnum;

    @Column(name = "rating", unique = false, updatable = true, nullable = false)
    private Integer rating = 0;
 
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seasonInformation")
    private List<EpisodeInformation> episode = new ArrayList<EpisodeInformation>();

    public String getSeasonnum() {
        return seasonnum;
    }

    public void setSeasonnum(String seasonnum) {
        this.seasonnum = seasonnum;
    }

    public List<EpisodeInformation> getEpisode() {
        return episode;
    }

    public void setEpisode(List<EpisodeInformation> episode) {
        this.episode = episode;
    }

    public SeriesInformation getSeriesInformation() {
        return seriesInformation;
    }

    public void setSeriesInformation(SeriesInformation seriesInformation) {
        this.seriesInformation = seriesInformation;
    }

    public Integer getSeasonid() {
        return seasonid;
    }

    public void setSeasonid(Integer seasonid) {
        this.seasonid = seasonid;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

}
