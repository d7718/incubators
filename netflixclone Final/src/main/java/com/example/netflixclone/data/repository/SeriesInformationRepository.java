package com.example.netflixclone.data.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.netflixclone.data.model.SeriesInformation;

public interface SeriesInformationRepository extends JpaRepository<SeriesInformation, Integer> {

    ArrayList<SeriesInformation> findAll();

    Optional<SeriesInformation> findById(Integer id);

    Optional<SeriesInformation> findBySeriesname(String seriesname);

    ArrayList<SeriesInformation> findBySeriesnameContaining(String string, Sort by);

    ArrayList<SeriesInformation> findByCategoryContaining(String string, Sort by);

}
