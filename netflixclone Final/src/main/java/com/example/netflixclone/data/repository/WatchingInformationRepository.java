package com.example.netflixclone.data.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.WatchingInformation;

public interface WatchingInformationRepository extends JpaRepository<WatchingInformation, Integer> {

    @Query(value = "DELETE FROM watching_info where watching = :watching", nativeQuery = true)
    @Modifying
    @Transactional
    public void deleteByWatching(Integer watching);

}
