package com.example.netflixclone.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.netflixclone.data.model.MovieRating;

public interface MovieRatingRepository extends JpaRepository<MovieRating, Integer> {

    @Query(value = "SELECT* FROM movie_rating mrating WHERE mrating.userid = :userid AND mrating.movieid = :movieid", nativeQuery = true)

    public Optional<MovieRating> findByUseridAndMovieid(Integer userid, Integer movieid);
}
