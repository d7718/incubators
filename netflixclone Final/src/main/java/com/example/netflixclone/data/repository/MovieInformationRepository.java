package com.example.netflixclone.data.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.netflixclone.data.model.MovieInformation;

public interface MovieInformationRepository extends JpaRepository<MovieInformation, Integer> {

    // @Query(value = "SELECT* FROM movie_info movie WHERE movie.id = :id ,
    // nativeQuery = true")
    // MovieInformation findById(Integer id);

    ArrayList<MovieInformation> findAll();

    Optional<MovieInformation> findById(Integer id);

    Optional<MovieInformation> findByMoviename(String moviename);

    ArrayList<MovieInformation> findByMovienameContaining(String string, Sort sort);

    ArrayList<MovieInformation> findByCategoryContaining(String string, Sort by);

}
