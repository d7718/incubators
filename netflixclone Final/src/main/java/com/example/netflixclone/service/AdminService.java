package com.example.netflixclone.service;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.netflixclone.data.model.EpisodeInformation;
import com.example.netflixclone.data.model.MovieInformation;
import com.example.netflixclone.data.model.SeasonInformation;
import com.example.netflixclone.data.model.SeriesInformation;
import com.example.netflixclone.data.repository.EpisodeInformationRepository;
import com.example.netflixclone.data.repository.MovieInformationRepository;
import com.example.netflixclone.data.repository.SeasonInformationRepository;
import com.example.netflixclone.data.repository.SeriesInformationRepository;
import com.example.netflixclone.data.repository.WatchingInformationRepository;
import com.example.netflixclone.dto.adminDto.AddEpisodeRequest;
import com.example.netflixclone.dto.adminDto.AddMovieRequest;
import com.example.netflixclone.dto.adminDto.AddSeasonRequest;
import com.example.netflixclone.dto.adminDto.AddSeriesRequest;
import com.example.netflixclone.dto.adminDto.EditEpisodeRequest;
import com.example.netflixclone.dto.adminDto.EditMovieRequest;
import com.example.netflixclone.dto.adminDto.EditSeriesRequest;

@Service
public class AdminService {

    @Autowired
    private MovieInformationRepository movieInformationRepository;

    @Autowired
    private SeriesInformationRepository seriesInformationRepository;

    @Autowired
    private SeasonInformationRepository seasonInformationRepository;

    @Autowired
    private EpisodeInformationRepository episodeInformationRepository;

    @Autowired
    private WatchingInformationRepository watchingInformationRepository;

    public ResponseEntity<String> addMovie(AddMovieRequest addMovieRequest) {

        // Optional<MovieInformation> movie = movieInformationRepository
        // .findByMoviename((String) entity.get("moviename"));

        // if (movie.isPresent()) {
        // return ResponseEntity.ok().body("Movie already exist!");
        // }

        // MovieInformation movieInfo = new MovieInformation((String)
        // entity.get("moviename"),
        // (String) entity.get("description"), (String) entity.get("category"),
        // (String) entity.get("trailer"), (String) entity.get("poster"), (String)
        // entity.get("movie"));

        // movieInformationRepository.save(movieInfo);

        Optional<MovieInformation> movie = movieInformationRepository
                .findByMoviename(addMovieRequest.getMoviename());

        if (movie.isPresent()) {
            return ResponseEntity.ok().body("Movie already exist!");
        }

        MovieInformation movieInfo = new MovieInformation(addMovieRequest.getMoviename(),
                addMovieRequest.getDescription(), addMovieRequest.getCategory(),
                addMovieRequest.getTrailer(), addMovieRequest.getPoster(), addMovieRequest.getMovie());

        movieInformationRepository.save(movieInfo);

        return ResponseEntity.ok().body("Movie added successfully.");
    }

    public ResponseEntity<String> editMovie(EditMovieRequest editMovieRequest) {

        // Optional<MovieInformation> exmovie = movieInformationRepository
        // .findById((Integer) entity.get("movieid"));

        // MovieInformation movie = exmovie.get();

        // if (!((String) entity.get("moviename")).equals(movie.getMoviename())) {

        // Optional<MovieInformation> check = movieInformationRepository
        // .findByMoviename((String) entity.get("moviename"));

        // if (check.isPresent())
        // return ResponseEntity.ok().body("Movie name must be unique.");
        // }

        // movie.setMoviename((String) entity.get("moviename"));
        // movie.setDescription((String) entity.get("description"));
        // movie.setCategory((String) entity.get("category"));
        // movie.setTrailer((String) entity.get("trailer"));
        // movie.setPoster((String) entity.get("poster"));
        // movie.setMovie((String) entity.get("movie"));

        // movieInformationRepository.save(movie);

        Optional<MovieInformation> exmovie = movieInformationRepository
                .findById(editMovieRequest.getMovieid());

        MovieInformation movie = exmovie.get();

        if (!(editMovieRequest.getMoviename()).equals(movie.getMoviename())) {

            Optional<MovieInformation> check = movieInformationRepository
                    .findByMoviename(editMovieRequest.getMoviename());

            if (check.isPresent())
                return ResponseEntity.ok().body("Movie name must be unique.");
        }

        movie.setMoviename(editMovieRequest.getMoviename());
        movie.setDescription(editMovieRequest.getDescription());
        movie.setCategory(editMovieRequest.getCategory());
        movie.setTrailer(editMovieRequest.getTrailer());
        movie.setPoster(editMovieRequest.getPoster());
        movie.setMovie(editMovieRequest.getMovie());

        movieInformationRepository.save(movie);

        return ResponseEntity.ok().body("Movie Information Updated Successfully!");
    }

    public ResponseEntity<String> deleteMovie(Map<String, Integer> entity) {

        movieInformationRepository.deleteById(entity.get("movieid"));
        watchingInformationRepository.deleteByWatching(entity.get("movieid"));

        return ResponseEntity.ok().body("Movie deleted successfully!");

    }

    public ResponseEntity<String> addSeries(AddSeriesRequest addSeriesRequest) {

        // Optional<SeriesInformation> series = seriesInformationRepository
        // .findBySeriesname((String) entity.get("seriesname"));

        // if (series.isPresent()) {
        // return ResponseEntity.ok().body("Series already exist!");
        // }

        // SeriesInformation seriesInfo = new SeriesInformation((String)
        // entity.get("seriesname"),
        // (String) entity.get("description"), (String) entity.get("category"),
        // (String) entity.get("trailer"), (String) entity.get("poster"));

        // seriesInformationRepository.save(seriesInfo);

        Optional<SeriesInformation> series = seriesInformationRepository
                .findBySeriesname(addSeriesRequest.getSeriesname());

        if (series.isPresent()) {
            return ResponseEntity.ok().body("Series already exist!");
        }

        SeriesInformation seriesInfo = new SeriesInformation(addSeriesRequest.getSeriesname(),
                addSeriesRequest.getDescription(), addSeriesRequest.getCategory(), addSeriesRequest.getTrailer(),
                addSeriesRequest.getPoster());

        seriesInformationRepository.save(seriesInfo);

        return ResponseEntity.ok().body("Series added successfully.");
    }

    public ResponseEntity<String> editSeries(EditSeriesRequest editSeriesRequest) {

        // Optional<SeriesInformation> exseries = seriesInformationRepository
        // .findById((Integer) entity.get("seriesid"));

        // SeriesInformation series = exseries.get();

        // if (!((String) entity.get("seriesname")).equals(series.getSeriesname())) {

        // Optional<SeriesInformation> check = seriesInformationRepository
        // .findBySeriesname((String) entity.get("seriesname"));

        // if (check.isPresent())
        // return ResponseEntity.ok().body("Series name must be unique.");
        // }

        // series.setSeriesname((String) entity.get("seriesname"));
        // series.setDescription((String) entity.get("description"));
        // series.setCategory((String) entity.get("category"));
        // series.setTrailer((String) entity.get("trailer"));
        // series.setPoster((String) entity.get("poster"));

        // seriesInformationRepository.save(series);

        Optional<SeriesInformation> exseries = seriesInformationRepository
                .findById(editSeriesRequest.getSeriesid());

        SeriesInformation series = exseries.get();

        if (!(editSeriesRequest.getSeriesname()).equals(series.getSeriesname())) {

            Optional<SeriesInformation> check = seriesInformationRepository
                    .findBySeriesname(editSeriesRequest.getSeriesname());

            if (check.isPresent())
                return ResponseEntity.ok().body("Series name must be unique.");
        }

        series.setSeriesname(editSeriesRequest.getSeriesname());
        series.setDescription(editSeriesRequest.getDescription());
        series.setCategory(editSeriesRequest.getCategory());
        series.setTrailer(editSeriesRequest.getTrailer());
        series.setPoster(editSeriesRequest.getPoster());

        seriesInformationRepository.save(series);
        return ResponseEntity.ok().body("Series Information Updated Successfully!");
    }

    public ResponseEntity<String> deleteSeries(Map<String, Integer> entity) {

        seriesInformationRepository.deleteById(entity.get("seriesid"));

        return ResponseEntity.ok().body("Series deleted successfully!");
    }

    public ResponseEntity<String> addSeason(AddSeasonRequest addSeasonRequest) {

        /*
         * parameters:
         * "seriesid":Integer
         * "seasonnum":String
         */

        // Optional<SeasonInformation> season = seasonInformationRepository
        // .findBySeriesidAndSeasonnum((Integer) entity.get("seriesid"), (String)
        // entity.get("seasonnum"));

        // if (season.isPresent()) {
        // return ResponseEntity.ok().body("Season already exist!");
        // }

        // Optional<SeriesInformation> series = seriesInformationRepository
        // .findById((Integer) entity.get("seriesid"));

        // SeriesInformation seriesObj = series.get();

        // SeasonInformation newSeason = new SeasonInformation();
        // newSeason.setSeasonnum((String) entity.get("seasonnum"));
        // newSeason.setSeriesInformation(seriesObj);
        // seriesObj.getSeason().add(newSeason);
        // seriesInformationRepository.save(seriesObj);

        Optional<SeasonInformation> season = seasonInformationRepository
                .findBySeriesidAndSeasonnum(addSeasonRequest.getSeriesid(), addSeasonRequest.getSeasonnum());

        if (season.isPresent()) {
            return ResponseEntity.ok().body("Season already exist!");
        }

        Optional<SeriesInformation> series = seriesInformationRepository
                .findById(addSeasonRequest.getSeriesid());

        SeriesInformation seriesObj = series.get();

        SeasonInformation newSeason = new SeasonInformation();
        newSeason.setSeasonnum(addSeasonRequest.getSeasonnum());
        newSeason.setSeriesInformation(seriesObj);
        seriesObj.getSeason().add(newSeason);
        seriesInformationRepository.save(seriesObj);

        return ResponseEntity.ok().body("Season added successfully.");
    }

    // public ResponseEntity<String> editSeason(Map entity) {

    // Optional<SeasonInformation> exseason = seasonInformationRepository
    // .findById((Integer) entity.get("seasonid"));

    // SeasonInformation season = exseason.get();

    // if (!((String) entity.get("seasonnum")).equals(season.getSeasonnum())) {

    // Optional<SeasonInformation> check = seasonInformationRepository
    // .findBySeriesidAndSeasonnum((Integer) entity.get("seriesid"), (String)
    // entity.get("seasonnum"));

    // if (check.isPresent())
    // return ResponseEntity.ok().body("Season name must be unique.");
    // }

    // Optional<SeriesInformation> series = seriesInformationRepository
    // .findById((Integer) entity.get("seriesid"));

    // SeriesInformation seriesObj = series.get();

    // season.setSeasonnum((String) entity.get("seasonnum"));
    // season.setSeriesInformation(seriesObj);
    // seriesObj.getSeason().add(season);
    // seriesInformationRepository.save(seriesObj);

    // return ResponseEntity.ok().body("Season Information Updated Successfully!");
    // }

    public ResponseEntity<String> deleteSeason(Map<String, Integer> entity) {

        seasonInformationRepository.deleteById((Integer) entity.get("seasonid"));

        return ResponseEntity.ok().body("Season deleted successfully!");
    }

    public ResponseEntity<String> addEpisode(AddEpisodeRequest addEpisodeRequest) {

        // int episodenum = Integer.parseInt((String) (entity.get("episodenum")));

        // Optional<EpisodeInformation> episode = episodeInformationRepository
        // .findBySeasonidAndEpisodenum((Integer) entity.get("seasonid"), episodenum);

        // if (episode.isPresent()) {
        // return ResponseEntity.ok().body("Episode already exist in current season!");
        // }

        // Optional<SeasonInformation> season = seasonInformationRepository
        // .findById((Integer) entity.get("seasonid"));

        // SeasonInformation seasonObj = season.get();

        // EpisodeInformation newEpisode = new EpisodeInformation(episodenum,
        // (String) entity.get("episodename"), (String) entity.get("description"),
        // (String) entity.get("poster"),
        // (String) entity.get("episode"));

        // newEpisode.setSeasonInformation(seasonObj);
        // seasonObj.getEpisode().add(newEpisode);
        // seasonInformationRepository.save(seasonObj);

        int episodenum = Integer.parseInt(addEpisodeRequest.getEpisodenum());

        Optional<EpisodeInformation> episode = episodeInformationRepository
                .findBySeasonidAndEpisodenum(addEpisodeRequest.getSeasonid(), episodenum);

        if (episode.isPresent()) {
            return ResponseEntity.ok().body("Episode already exist in current season!");
        }

        Optional<SeasonInformation> season = seasonInformationRepository
                .findById(addEpisodeRequest.getSeasonid());

        SeasonInformation seasonObj = season.get();

        EpisodeInformation newEpisode = new EpisodeInformation(episodenum,
                addEpisodeRequest.getEpisodename(), addEpisodeRequest.getDescription(), addEpisodeRequest.getPoster(),
                addEpisodeRequest.getEpisode());

        newEpisode.setSeasonInformation(seasonObj);
        seasonObj.getEpisode().add(newEpisode);
        seasonInformationRepository.save(seasonObj);

        return ResponseEntity.ok().body("Episode added successfully.");
    }

    public ResponseEntity<String> editEpisode(EditEpisodeRequest editEpisodeRequest) {

        // Optional<EpisodeInformation> exepisode = episodeInformationRepository
        // .findById((Integer) entity.get("episodeid"));

        // EpisodeInformation episode = exepisode.get();

        // Integer episodenum = Integer.parseInt((String) entity.get("episodenum"));
        // if (!(episodenum == (episode.getEpisodenum()))) {

        // Optional<EpisodeInformation> check = episodeInformationRepository
        // .findBySeasonidAndEpisodenum((Integer) entity.get("seasonid"), episodenum);

        // if (check.isPresent())
        // return ResponseEntity.ok().body("Episode number must be unique.");
        // }

        // if (!((String) entity.get("episodename")).equals(episode.getEpisodename())) {

        // Optional<EpisodeInformation> check = episodeInformationRepository
        // .findBySeasonidAndEpisodename((Integer) entity.get("seasonid"), (String)
        // entity.get("episodename"));

        // if (check.isPresent())
        // return ResponseEntity.ok().body("Episode name must be unique.");
        // }

        // // Optional<SeasonInformation> season =
        // // seasonInformationRepository.findById((Integer) entity.get("seasonid"));

        // // SeasonInformation seasonObj = season.get();

        // // Optional<SeriesInformation> series = seriesInformationRepository
        // // .findById(seasonObj.getSeriesInformation().getId());
        // // SeriesInformation seriesObj = series.get();

        // episode.setEpisodenum(episodenum);
        // episode.setEpisodename((String) entity.get("episodename"));
        // episode.setDescription((String) entity.get("description"));
        // episode.setPoster((String) entity.get("poster"));
        // episode.setEpisode((String) entity.get("episode"));
        // // episode.setSeasonInformation(seasonObj);
        // // seasonObj.getEpisode().add(episode);
        // // seasonObj.setSeriesInformation(seriesObj);
        // // seasonInformationRepository.save(seasonObj);
        // // seriesObj.getSeason().add(seasonObj);
        // // seriesInformationRepository.save(seriesObj);
        // episodeInformationRepository.save(episode);

        Optional<EpisodeInformation> exepisode = episodeInformationRepository
                .findById(editEpisodeRequest.getEpisodeid());

        EpisodeInformation episode = exepisode.get();

        Integer episodenum = Integer.parseInt(editEpisodeRequest.getEpisodenum());
        if (!(episodenum == (episode.getEpisodenum()))) {

            Optional<EpisodeInformation> check = episodeInformationRepository
                    .findBySeasonidAndEpisodenum(editEpisodeRequest.getSeasonid(), episodenum);

            if (check.isPresent())
                return ResponseEntity.ok().body("Episode number must be unique.");
        }

        if (!(editEpisodeRequest.getEpisodename()).equals(episode.getEpisodename())) {

            Optional<EpisodeInformation> check = episodeInformationRepository
                    .findBySeasonidAndEpisodename(editEpisodeRequest.getSeasonid(),
                            editEpisodeRequest.getEpisodename());

            if (check.isPresent())
                return ResponseEntity.ok().body("Episode name must be unique.");
        }

        episode.setEpisodenum(episodenum);
        episode.setEpisodename(editEpisodeRequest.getEpisodename());
        episode.setDescription(editEpisodeRequest.getDescription());
        episode.setPoster(editEpisodeRequest.getPoster());
        episode.setEpisode(editEpisodeRequest.getEpisode());
        episodeInformationRepository.save(episode);

        return ResponseEntity.ok().body("Episode Information Updated Successfully!");

    }

    public ResponseEntity<String> deleteEpisode(Map<String, Integer> entity) {

        episodeInformationRepository.deleteById((Integer) entity.get("episodeid"));

        return ResponseEntity.ok().body("Episode deleted successfully!");
    }
}
