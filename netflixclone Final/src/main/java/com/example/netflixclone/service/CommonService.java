package com.example.netflixclone.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.netflixclone.data.model.EpisodeInformation;
import com.example.netflixclone.data.model.MovieInformation;
import com.example.netflixclone.data.model.SeasonInformation;
import com.example.netflixclone.data.model.SeriesInformation;
import com.example.netflixclone.data.model.UserInformation;
import com.example.netflixclone.data.repository.MovieInformationRepository;
import com.example.netflixclone.data.repository.SeasonInformationRepository;
import com.example.netflixclone.data.repository.SeriesInformationRepository;
import com.example.netflixclone.data.repository.UserInformationRepository;
import com.example.netflixclone.dto.commonDto.LogInRequest;
import com.example.netflixclone.dto.commonDto.SignUpRequest;
import com.example.netflixclone.dto.commonDto.UpdatePasswordRequest;

@Service
public class CommonService {

    @Autowired
    private UserInformationRepository userInformationRepository;

    @Autowired
    private MovieInformationRepository movieInformationRepository;

    @Autowired
    private SeriesInformationRepository seriesInformationRepository;

    @Autowired
    private SeasonInformationRepository seasonInformationRepository;

    @Autowired
    private UserService userService;

    public ResponseEntity<String> checkMail(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findByEmail(entity.get("email"));

        if (user.isPresent()) {
            return ResponseEntity.ok().body("true");
        }
        return ResponseEntity.ok().body("false");
    }

    public ResponseEntity<String> signUp(SignUpRequest signUpRequest) {

        // UserInformation userInformation = new UserInformation(entity.get("name"),
        // entity.get("phonenumber"),
        // entity.get("question"), entity.get("answer"), entity.get("password"),
        // entity.get("email"), 1);

        // userInformationRepository.save(userInformation);

        UserInformation userInformation = new UserInformation(signUpRequest.getName(), signUpRequest.getPhonenumber(),
                signUpRequest.getQuestion(), signUpRequest.getAnswer(), signUpRequest.getPassword(),
                signUpRequest.getEmail(), 1);

        userInformationRepository.save(userInformation);

        return ResponseEntity.ok().body("You are signed up successfully");
    }

    public ResponseEntity<Map<String, String>> logIn(LogInRequest logInRequest) {

        // int flag = 0;

        // Optional<UserInformation> emailUser =
        // userInformationRepository.findByEmail(entity.get("username"));
        // Optional<UserInformation> phonenumberUser =
        // userInformationRepository.findByPhonenumber(entity.get("username"));

        // Map<String, String> response = new HashMap<String, String>();

        // if (!emailUser.isPresent() && !phonenumberUser.isPresent()) {
        // response.put("message", "User not registered.");
        // return ResponseEntity.ok().body(response);
        // }

        // UserInformation userObj = null;
        // // ArrayList<MovieInformation> movies = null;
        // // ArrayList<SeriesInformation> series = null;
        // // ArrayList watchingList = null;

        // if (!emailUser.isPresent())
        // userObj = phonenumberUser.get();
        // else
        // userObj = emailUser.get();

        // if (userObj.getLoginStatus() == 1 && flag == 0) {
        // response.put("message", "Already Logged in");
        // flag = 1;
        // }
        // if (!userObj.getPassword().equals(entity.get("password")) && flag == 0) {
        // response.put("message", "Invalid password");
        // flag = 1;
        // }

        // if (flag == 0) {
        // userObj.setLoginStatus(1);
        // response.put("message", "Logged in successfully");
        // response.put("admin", userObj.getAdmin().toString());
        // /*
        // * movies = movieInformationRepository.findAll();
        // * series = seriesInformationRepository.findAll();
        // */// watchingList = FetchObjects.getWatchingList(userObj.getWatchingList());
        // userInformationRepository.save(userObj);
        // /*
        // * response.put("movies", movies);
        // * response.put("series", series);
        // * response.put("watchingList", watchingList);
        // */
        // }

        int flag = 0;

        Optional<UserInformation> emailUser = userInformationRepository.findByEmail(logInRequest.getUsername());
        Optional<UserInformation> phonenumberUser = userInformationRepository
                .findByPhonenumber(logInRequest.getUsername());

        Map<String, String> response = new HashMap<String, String>();

        if (!emailUser.isPresent() && !phonenumberUser.isPresent()) {
            response.put("message", "User not registered.");
            return ResponseEntity.ok().body(response);
        }

        UserInformation userObj = null;

        if (!emailUser.isPresent())
            userObj = phonenumberUser.get();
        else
            userObj = emailUser.get();

        if (!userObj.getPassword().equals(logInRequest.getPassword()) && flag == 0) {
            response.put("message", "Invalid password");
            flag = 1;
        }

        if (userObj.getLoginStatus() == 1 && flag == 0) {
            response.put("message", "Already Logged in");
            flag = 1;
        }

        if (flag == 0) {
            userObj.setLoginStatus(1);
            response.put("message", "Logged in successfully");
            response.put("admin", userObj.getAdmin().toString());
            userInformationRepository.save(userObj);
        }

        return ResponseEntity.ok().body(response);

    }

    public ResponseEntity<String> logOut(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findByEmail(entity.get("username"));
        UserInformation userObj = null;
        if (user.isPresent())
            userObj = user.get();
        else {
            user = userInformationRepository.findByPhonenumber(entity.get("username"));
            userObj = user.get();
        }
        userObj.setLoginStatus(0);
        userInformationRepository.save(userObj);

        return ResponseEntity.ok().body("Logged Out Successfully");

    }

    public ResponseEntity<String> getQuestion(Map<String, String> entity) {

        Optional<UserInformation> user = userInformationRepository.findByPhonenumber(entity.get("phonenumber"));

        if (!user.isPresent())
            return ResponseEntity.ok().body("User does not exist.");

        UserInformation us = user.get();

        return ResponseEntity.ok().body(us.getQuestion());

    }

    public ResponseEntity<String> updatePassword(UpdatePasswordRequest updatePasswordRequest) {

        // Optional<UserInformation> user =
        // userInformationRepository.findByPhonenumber(entity.get("phonenumber"));
        // UserInformation usObj = user.get();

        // if (!usObj.getAnswer().equalsIgnoreCase(entity.get("answer")))
        // return ResponseEntity.ok().body("Incorrect answer.");

        // if (usObj.getPassword().equals(entity.get("password")))
        // return ResponseEntity.ok().body("Password already used.");

        // usObj.setPassword(entity.get("password"));
        // userInformationRepository.save(usObj);

        Optional<UserInformation> user = userInformationRepository
                .findByPhonenumber(updatePasswordRequest.getPhonenumber());
        UserInformation usObj = user.get();

        if (!usObj.getAnswer().equalsIgnoreCase(updatePasswordRequest.getAnswer()))
            return ResponseEntity.ok().body("Incorrect answer.");

        if (usObj.getPassword().equals(updatePasswordRequest.getPassword()))
            return ResponseEntity.ok().body("Password already used.");

        usObj.setPassword(updatePasswordRequest.getPassword());
        userInformationRepository.save(usObj);

        return ResponseEntity.ok().body("Password Updated Successfully.");
    }

    public ResponseEntity<List<MovieInformation>> showMovies() {

        List<MovieInformation> movies = null;
        movies = movieInformationRepository.findAll(Sort.by(Sort.Direction.DESC, "rating"));
        return ResponseEntity.ok().body(movies);
    }

    public ResponseEntity<List<SeriesInformation>> showSeries() {
        List<SeriesInformation> series = null;
        series = seriesInformationRepository.findAll(Sort.by(Sort.Direction.DESC, "rating"));
        return ResponseEntity.ok().body(series);

    }

    public ResponseEntity<Map<String, Object>> showBySearch(Map<String, String> entity) {

        Map<String, Object> response = new HashMap<String, Object>();
        ArrayList<MovieInformation> movies = new ArrayList<>();
        ArrayList<SeriesInformation> series = new ArrayList<>();

        String search = entity.get("search");
        String[] words = search.split(" ");

        for (int i = 0; i < words.length; i++) {

            if (words[i] == "")
                continue;

            movies.addAll(movieInformationRepository.findByMovienameContaining(words[i],
                    Sort.by(Sort.Direction.DESC, "rating")));
            movies.addAll(movieInformationRepository.findByCategoryContaining(words[i],
                    Sort.by(Sort.Direction.DESC, "rating")));

            Set<MovieInformation> set1 = new LinkedHashSet<MovieInformation>(movies);
            movies = new ArrayList<MovieInformation>(set1);

            series.addAll(seriesInformationRepository.findBySeriesnameContaining(words[i],
                    Sort.by(Sort.Direction.DESC, "rating")));
            series.addAll(seriesInformationRepository.findByCategoryContaining(words[i],
                    Sort.by(Sort.Direction.DESC, "rating")));

            Set<SeriesInformation> set2 = new LinkedHashSet<SeriesInformation>(series);
            series = new ArrayList<SeriesInformation>(set2);

        }

        response.put("movies", movies);
        response.put("series", series);

        return ResponseEntity.ok().body(response);
    }

    // New -API

    public ResponseEntity<Map<String, Object>> showContent(Map<String, String> entity) {

        Optional<UserInformation> emailUser = userInformationRepository.findByEmail(entity.get("username"));
        Optional<UserInformation> phonenumberUser = userInformationRepository.findByPhonenumber(entity.get("username"));

        Map<String, Object> response = new HashMap<>();
        UserInformation userObj = null;
        ArrayList<MovieInformation> movies = null;
        ArrayList<SeriesInformation> series = null;
        ArrayList<Object> watchingList = null;

        if (!emailUser.isPresent())
            userObj = phonenumberUser.get();
        else
            userObj = emailUser.get();

        movies = movieInformationRepository.findAll();
        series = seriesInformationRepository.findAll();
        watchingList = userService.fetchWatchingList(userObj);

        response.put("userid",userObj.getId());
        response.put("movies", movies);
        response.put("series", series);
        response.put("watchingList", watchingList);
     

        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<MovieInformation> getMovie(Map<String, Integer> entity) {

        Optional<MovieInformation> mov = movieInformationRepository.findById((Integer) entity.get("id"));
        MovieInformation movie = mov.get();

        return ResponseEntity.ok().body(movie);

    }

    public ResponseEntity<SeriesInformation> getSeries(Map<String, Integer> entity) {
        Optional<SeriesInformation> ser = seriesInformationRepository.findById((Integer) entity.get("id"));
        SeriesInformation series = ser.get();

        return ResponseEntity.ok().body(series);
        // return ResponseEntity.ok().body(series.getSeason());
    }

    public ResponseEntity<List<SeasonInformation>> getSeasons(Map<String, Integer> entity) {

        Optional<SeriesInformation> ser = seriesInformationRepository.findById((Integer) entity.get("id"));
        SeriesInformation series = ser.get();

        // return ResponseEntity.ok().body(series);
        return ResponseEntity.ok().body(series.getSeason());

    }

    public ResponseEntity<List<EpisodeInformation>> getEpisodes(Map<String, Integer> entity) {

        Optional<SeasonInformation> se = seasonInformationRepository
                .findBySeriesidAndSeasonid((Integer) entity.get("id"), (Integer) entity.get("seasonid"));
        SeasonInformation season = se.get();

        List<EpisodeInformation> episodes = season.getEpisode();
        // if (episodes.isEmpty())
        // return ResponseEntity.ok().body("false");
        // return ResponseEntity.ok().body(season);

        return ResponseEntity.ok().body(episodes);
    }
}