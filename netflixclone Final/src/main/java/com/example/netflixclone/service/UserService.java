package com.example.netflixclone.service;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.netflixclone.data.model.EpisodeInformation;
import com.example.netflixclone.data.model.EpisodeRating;
import com.example.netflixclone.data.model.MovieInformation;
import com.example.netflixclone.data.model.MovieRating;
import com.example.netflixclone.data.model.SeasonInformation;
import com.example.netflixclone.data.model.SeriesInformation;
import com.example.netflixclone.data.model.SeriesRating;
import com.example.netflixclone.data.model.UserInformation;
import com.example.netflixclone.data.model.WatchingInformation;
import com.example.netflixclone.data.repository.EpisodeInformationRepository;
import com.example.netflixclone.data.repository.EpisodeRatingRepository;
import com.example.netflixclone.data.repository.MovieInformationRepository;
import com.example.netflixclone.data.repository.MovieRatingRepository;
import com.example.netflixclone.data.repository.SeasonInformationRepository;
import com.example.netflixclone.data.repository.SeriesInformationRepository;
import com.example.netflixclone.data.repository.SeriesRatingRepository;
import com.example.netflixclone.data.repository.UserInformationRepository;
import com.example.netflixclone.dto.UserDto.AddToWatchingListRequest;

@Service
public class UserService {

    @Autowired
    private UserInformationRepository userInformationRepository;

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    private EpisodeRatingRepository episodeRatingRepository;

    @Autowired
    private MovieInformationRepository movieInformationRepository;

    @Autowired
    private SeasonInformationRepository seasonInformationRepository;

    @Autowired
    private SeriesInformationRepository seriesInformationRepository;

    @Autowired
    private EpisodeInformationRepository episodeInformationRepository;

    @Autowired
    private SeriesRatingRepository seriesRatingRepository;

    public ResponseEntity<String> addToWatchingList(AddToWatchingListRequest addToWatchingListRequest) {

        // Optional<UserInformation> user = userInformationRepository.findById((Integer)
        // entity.get("userid"));
        // UserInformation userObj = user.get();

        // WatchingInformation watch = new WatchingInformation((Integer)
        // entity.get("watching"),
        // (Integer) entity.get("series"));

        // watch.setUserInformation(userObj);
        // userObj.getWatchingList().add(watch);

        // userInformationRepository.save(userObj);

        Optional<UserInformation> user = userInformationRepository.findById(addToWatchingListRequest.getUserid());
        UserInformation userObj = user.get();

        WatchingInformation watch = new WatchingInformation(addToWatchingListRequest.getWatching(),
                addToWatchingListRequest.getSeries());

        watch.setUserInformation(userObj);
        userObj.getWatchingList().add(watch);

        userInformationRepository.save(userObj);

        return ResponseEntity.ok().body("Added to WatchList");
    }

    public ResponseEntity<ArrayList<Object>> showWatchingList(Map<String, Integer> entity) {

        Optional<UserInformation> user = userInformationRepository.findById((Integer) entity.get("userid"));
        UserInformation userObj = user.get();
        ArrayList<Object> response = fetchWatchingList(userObj);

        return ResponseEntity.ok().body(response);

    }

    // public ResponseEntity<String> canReview(Map entity) {

    // if ((Integer) entity.get("series") == 0) {

    // Optional<MovieRating> rating =
    // movieRatingRepository.findByUseridAndMovieid((Integer) entity.get("userid"),
    // (Integer) entity.get("id"));

    // if (rating.isPresent())
    // return ResponseEntity.ok().body("false");
    // } else {
    // Optional<EpisodeRating> rating =
    // episodeRatingRepository.findByUseridAndEpisodeid(
    // (Integer) entity.get("userid"),
    // (Integer) entity.get("id"));

    // if (rating.isPresent())
    // return ResponseEntity.ok().body("false");
    // }

    // return ResponseEntity.ok().body("true");
    // }

    // public ResponseEntity<String> addReview(Map entity) {

    // if ((Integer) entity.get("series") == 0) {

    // MovieInformation movie = movieInformationRepository.findById((Integer)
    // entity.get("id")).get();

    // MovieRating rating = new MovieRating();

    // rating.setRating((Integer) entity.get("rating"));
    // rating.setReview((String) entity.get("review")); // Error might occur
    // rating.setUserid((Integer) entity.get("userid"));
    // rating.setMovieInformation(movie);
    // movie.getRatingList().add(rating);

    // ArrayList<MovieRating> calculateRating = new
    // ArrayList<MovieRating>(movie.getRatingList());

    // ListIterator<MovieRating> ll = calculateRating.listIterator();
    // int tempRating = 0;

    // if (!calculateRating.isEmpty()) {
    // while (ll.hasNext()) {
    // MovieRating temp = ll.next();
    // tempRating = tempRating + temp.getRating();
    // }

    // movie.setRating(tempRating / calculateRating.size());
    // }
    // movieInformationRepository.save(movie);

    // } else {

    // EpisodeInformation episode = episodeRatingRepository.findById((Integer)
    // entity.get("id")).get();

    // SeasonInformation season = seasonInformationRepository
    // .findById(episode.getSeasonInformation().getSeasonid()).get();

    // SeriesInformation series =
    // seriesInformationRepository.findById(season.getSeriesInformation().getId())
    // .get();

    // EpisodeRating rating = new EpisodeRating();

    // rating.setRating((Integer) entity.get("rating"));
    // rating.setReview((String) entity.get("review")); // Error might occur
    // rating.setUserid((Integer) entity.get("userid"));
    // rating.setEpisodeInformation(episode);
    // episode.getRatingList().add(rating);

    // ArrayList<EpisodeRating> calculateRatingForEpisode = new
    // ArrayList<EpisodeRating>(episode.getRatingList());

    // ListIterator<EpisodeRating> ll = calculateRatingForEpisode.listIterator();
    // int tempRating = 0;

    // if (!calculateRatingForEpisode.isEmpty()) {
    // while (ll.hasNext()) {
    // EpisodeRating temp = ll.next();
    // tempRating = tempRating + temp.getRating();
    // }

    // episode.setRating(tempRating / calculateRatingForEpisode.size());
    // }
    // episodeInformationRepository.save(episode);

    // ArrayList<EpisodeInformation> calculateRatingForSeason = new
    // ArrayList<EpisodeInformation>(
    // season.getEpisode());

    // ListIterator<EpisodeInformation> ll1 =
    // calculateRatingForSeason.listIterator();
    // tempRating = 0;

    // if (!calculateRatingForSeason.isEmpty()) {
    // while (ll1.hasNext()) {
    // EpisodeInformation temp = ll1.next();
    // tempRating = tempRating + temp.getRating();
    // // System.out.println(temp.getRating());
    // }

    // System.out.println(tempRating);
    // System.out.println(calculateRatingForSeason.size());

    // season.setRating(tempRating / calculateRatingForSeason.size());
    // episode.setSeasonInformation(season);
    // System.out.println(season.getRating());

    // }
    // //episode.setSeasonInformation(season);
    // season.getEpisode().add(episode);
    // season.setSeriesInformation(series);
    // series.getSeason().add(season);
    // seasonInformationRepository.save(season);
    // seriesInformationRepository.save(series);

    // }
    // return ResponseEntity.ok().body("Review added successfully");
    // }

    /*
     * {
     * ArrayList<EpisodeInformation> calculateRatingForSeason = new
     * ArrayList<EpisodeInformation>(
     * season.getEpisode());
     * 
     * ListIterator<EpisodeInformation> ll1 =
     * calculateRatingForSeason.listIterator();
     * tempRating = 0;
     * 
     * if (!calculateRatingForSeason.isEmpty()) {
     * while (ll1.hasNext()) {
     * EpisodeInformation temp = ll1.next();
     * tempRating = tempRating + temp.getRating();
     * }
     * 
     * series.setRating(tempRating / calculateRatingForSeason.size());
     * }
     * 
     * seriesInformationRepository.save(series);
     * }
     */

    public ResponseEntity<String> canReview(Map<String, Integer> entity) {

        if ((Integer) entity.get("series") == 0) {

            Optional<MovieRating> rating = movieRatingRepository.findByUseridAndMovieid((Integer) entity.get("userid"),
                    (Integer) entity.get("movieid"));

            if (rating.isPresent())
                return ResponseEntity.ok().body("false");
        } else {

            if (entity.containsKey("seriesid")) {

                Optional<SeriesRating> rating = seriesRatingRepository
                        .findByUseridAndSeriesid((Integer) entity.get("userid"), (Integer) entity.get("seriesid"));

                if (rating.isPresent())
                    return ResponseEntity.ok().body("false");

            } else {

                Optional<EpisodeRating> rating = episodeRatingRepository.findByUseridAndEpisodeid(
                        (Integer) entity.get("userid"),
                        (Integer) entity.get("episodeid"));

                if (rating.isPresent())
                    return ResponseEntity.ok().body("false");

            }
        }

        return ResponseEntity.ok().body("true");
    }

    public ResponseEntity<String> addReview(Map<String, ?> entity) {

        if ((Integer) entity.get("series") == 0) {

            MovieInformation movie = movieInformationRepository.findById((Integer) entity.get("movieid")).get();

            MovieRating rating = new MovieRating();

            rating.setRating((Integer) entity.get("rating"));
            rating.setReview((String) entity.get("review")); // Error might occur
            rating.setUserid((Integer) entity.get("userid"));
            rating.setUsername((String) entity.get("username"));
            rating.setMovieInformation(movie);
            movie.getRatingList().add(rating);

            ArrayList<MovieRating> calculateRating = new ArrayList<MovieRating>(movie.getRatingList());

            ListIterator<MovieRating> ll = calculateRating.listIterator();
            int tempRating = 0;

            if (!calculateRating.isEmpty()) {
                while (ll.hasNext()) {
                    MovieRating temp = ll.next();
                    tempRating = tempRating + temp.getRating();
                }

                movie.setRating(tempRating / calculateRating.size());
            }
            movieInformationRepository.save(movie);

        } else {

            if (entity.containsKey("seriesid")) {

                SeriesInformation series = seriesInformationRepository.findById((Integer) (entity.get("seriesid")))
                        .get();

                SeriesRating seriesRating = new SeriesRating();

                seriesRating.setRating((Integer) entity.get("rating"));
                seriesRating.setReview((String) entity.get("review")); // Error might occur
                seriesRating.setUserid((Integer) entity.get("userid"));
                seriesRating.setUsername((String) entity.get("username"));
                seriesRating.setSeriesInformation(series);
                series.getRatingList().add(seriesRating);

                ArrayList<SeriesRating> calculateRatingForSeries = new ArrayList<SeriesRating>(series.getRatingList());
                ListIterator<SeriesRating> listIterator = calculateRatingForSeries.listIterator();
                int tempRating = 0;

                if (!calculateRatingForSeries.isEmpty()) {
                    while (listIterator.hasNext()) {
                        SeriesRating temp = listIterator.next();
                        tempRating = tempRating + temp.getRating();
                    }

                    series.setRating(tempRating / calculateRatingForSeries.size());
                }
                seriesInformationRepository.save(series);

            } else {

                EpisodeInformation episode = episodeRatingRepository.findById((Integer) entity.get("episodeid")).get();

                SeasonInformation season = seasonInformationRepository
                        .findById(episode.getSeasonInformation().getSeasonid()).get();

                EpisodeRating rating = new EpisodeRating();

                rating.setRating((Integer) entity.get("rating"));
                rating.setReview((String) entity.get("review")); // Error might occur
                rating.setUserid((Integer) entity.get("userid"));
                rating.setUsername((String) entity.get("username"));

                rating.setEpisodeInformation(episode);
                episode.getRatingList().add(rating);

                ArrayList<EpisodeRating> calculateRatingForEpisode = new ArrayList<EpisodeRating>(
                        episode.getRatingList());

                ListIterator<EpisodeRating> ll = calculateRatingForEpisode.listIterator();
                int tempRating = 0;

                if (!calculateRatingForEpisode.isEmpty()) {
                    while (ll.hasNext()) {
                        EpisodeRating temp = ll.next();
                        tempRating = tempRating + temp.getRating();
                    }

                    episode.setRating(tempRating / calculateRatingForEpisode.size());
                }
                episodeInformationRepository.save(episode);

                ArrayList<EpisodeInformation> calculateRatingForSeason = new ArrayList<EpisodeInformation>(
                        season.getEpisode());

                ListIterator<EpisodeInformation> ll1 = calculateRatingForSeason.listIterator();
                tempRating = 0;

                if (!calculateRatingForSeason.isEmpty()) {
                    while (ll1.hasNext()) {
                        EpisodeInformation temp = ll1.next();
                        tempRating = tempRating + temp.getRating();
                        // System.out.println(temp.getRating());
                    }

                    season.setRating(tempRating / calculateRatingForSeason.size());
                }
                episode.setSeasonInformation(season);
                // season.setSeriesInformation(series);
                // series.getSeason().add(season);
                season.getEpisode().add(episode);

                seasonInformationRepository.save(season);
                // seriesInformationRepository.save(series);

            }

        }

        return ResponseEntity.ok().body("Review added successfully");

    }

    // show reviews

    public ResponseEntity<List<?>> showReviews(Map<String, Integer> entity) {

        List<EpisodeRating> episodeRatings = null;

        if ((Integer) entity.get("series") == 0) {

            MovieInformation movie = movieInformationRepository.findById((Integer) entity.get("movieid")).get();

            List<MovieRating> movieRatings = movie.getRatingList();

            return ResponseEntity.ok().body(movieRatings);

        } else {

            if (entity.containsKey("seriesid")) {

                SeriesInformation series = seriesInformationRepository.findById((Integer) (entity.get("seriesid")))
                        .get();

                List<SeriesRating> seriesRatings = series.getRatingList();

                return ResponseEntity.ok().body(seriesRatings);

            } else {

                EpisodeInformation episode = episodeRatingRepository.findById((Integer) entity.get("episodeid")).get();

                episodeRatings = (episode.getRatingList());

            }

            return ResponseEntity.ok().body(episodeRatings);

        }
    }

    // Method To Fetch Objects

    public ArrayList<Object> fetchWatchingList(UserInformation user) {

        List<WatchingInformation> watching = user.getWatchingList();
        ArrayList<Object> response = new ArrayList<>();
        ListIterator<WatchingInformation> ll = watching.listIterator();

        while (ll.hasNext()) {

            WatchingInformation watch = ll.next();
            MovieInformation toAddMovie = null;
            SeriesInformation toAddSeries = null;

            if (watch.getSeries() == 0) {
                Optional<MovieInformation> optionalMovie = movieInformationRepository.findById(watch.getWatching());
                if (optionalMovie.isPresent()) {
                    toAddMovie = optionalMovie.get();
                    response.add(toAddMovie);
                }
            } else {
                Optional<SeriesInformation> optionalSeries = seriesInformationRepository.findById(watch.getWatching());
                if (optionalSeries.isPresent()) {
                    toAddSeries = optionalSeries.get();
                    response.add(toAddSeries);
                }
            }

        }

        return response;

    }

}
