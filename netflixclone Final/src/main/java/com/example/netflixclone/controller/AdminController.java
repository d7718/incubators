package com.example.netflixclone.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.netflixclone.dto.adminDto.AddEpisodeRequest;
import com.example.netflixclone.dto.adminDto.AddMovieRequest;
import com.example.netflixclone.dto.adminDto.AddSeasonRequest;
import com.example.netflixclone.dto.adminDto.AddSeriesRequest;
import com.example.netflixclone.dto.adminDto.EditEpisodeRequest;
import com.example.netflixclone.dto.adminDto.EditMovieRequest;
import com.example.netflixclone.dto.adminDto.EditSeriesRequest;
import com.example.netflixclone.service.AdminService;

@RestController
@RequestMapping(value = "/netflix")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping(value = "/add-movie")
    public ResponseEntity<String> addMovie(@Valid @RequestBody AddMovieRequest addMovieRequest) {
        return adminService.addMovie(addMovieRequest);
    }

    @PostMapping(value = "/edit-movie")
    public ResponseEntity<String> editMovie(@Valid @RequestBody EditMovieRequest editMovieRequest) {
        return adminService.editMovie(editMovieRequest);
    }

    @DeleteMapping(value = "/delete-movie")
    public ResponseEntity<String> deleteMovie(@RequestBody Map<String, Integer> entity) {
        return adminService.deleteMovie(entity);
    }

    @PostMapping(value = "/add-series")
    public ResponseEntity<String> addSeries(@Valid @RequestBody AddSeriesRequest addSeriesRequest) {
        return adminService.addSeries(addSeriesRequest);
    }

    @PostMapping(value = "/edit-series")
    public ResponseEntity<String> editSeries(@Valid @RequestBody EditSeriesRequest editSeriesRequest) {
        return adminService.editSeries(editSeriesRequest);
    }

    @DeleteMapping(value = "/delete-series")
    public ResponseEntity<String> deleteSeries(@RequestBody Map<String, Integer> entity) {
        return adminService.deleteSeries(entity);
    }

    @PostMapping(value = "/add-season")
    public ResponseEntity<String> addSeason(@Valid @RequestBody AddSeasonRequest addSeasonRequest) {
        return adminService.addSeason(addSeasonRequest);
    }

    // @PostMapping(value = "/edit-season")
    // public ResponseEntity<String> editSeason(@RequestBody Map entity) {
    // return adminService.editSeason(entity);
    // }

    @DeleteMapping(value = "/delete-season")
    public ResponseEntity<String> deleteSeason(@RequestBody Map<String, Integer> entity) {
        return adminService.deleteSeason(entity);
    }

    @PostMapping(value = "/add-episode")
    public ResponseEntity<String> addEpisode(@Valid @RequestBody AddEpisodeRequest addEpisodeRequest) {
        return adminService.addEpisode(addEpisodeRequest);
    }

    @PostMapping(value = "/edit-episode")
    public ResponseEntity<String> editEpisode(@Valid @RequestBody EditEpisodeRequest editEpisodeRequest) {
        return adminService.editEpisode(editEpisodeRequest);
    }

    @DeleteMapping(value = "/delete-episode")
    public ResponseEntity<String> deleteEpisode(@RequestBody Map<String, Integer> entity) {
        return adminService.deleteEpisode(entity);
    }
}
