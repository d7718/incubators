package com.example.netflixclone.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.netflixclone.dto.UserDto.AddToWatchingListRequest;
import com.example.netflixclone.service.UserService;

@RestController
@RequestMapping(value = "/netflix")
public class UserController {

    @Autowired
    private UserService userService;

    // New API
    @PostMapping("/add-to-watching-list")
    public ResponseEntity<String> addToWatchingList(
            @Valid @RequestBody AddToWatchingListRequest addToWatchingListRequest) {

        return userService.addToWatchingList(addToWatchingListRequest);
    }

    @PostMapping("/show-watching-list")
    public ResponseEntity<ArrayList<Object>> showWatchingList(@RequestBody Map<String, Integer> entity) {
        return userService.showWatchingList(entity);
    }

    @PostMapping("/can-review")
    public ResponseEntity<String> carReview(@RequestBody Map<String, Integer> entity) {
        return userService.canReview(entity);
    }

    @PostMapping("/add-review")
    public ResponseEntity<String> addReview(@RequestBody Map<String, ?> entity) {
        return userService.addReview(entity);
    }

    @PostMapping("/show-reviews")
    public ResponseEntity<List<?>> showReviews(@RequestBody Map<String, Integer> entity) {

        return userService.showReviews(entity);
    }

}