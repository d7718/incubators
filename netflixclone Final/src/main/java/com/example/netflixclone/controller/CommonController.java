package com.example.netflixclone.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.netflixclone.data.model.EpisodeInformation;
import com.example.netflixclone.data.model.MovieInformation;
import com.example.netflixclone.data.model.SeasonInformation;
import com.example.netflixclone.data.model.SeriesInformation;
import com.example.netflixclone.dto.commonDto.LogInRequest;
import com.example.netflixclone.dto.commonDto.SignUpRequest;
import com.example.netflixclone.dto.commonDto.UpdatePasswordRequest;
import com.example.netflixclone.service.CommonService;

@RestController
@RequestMapping(value = "/netflix")
public class CommonController {

    @Autowired
    private CommonService commonService;

    @PostMapping(value = "/check-mail")
    public ResponseEntity<String> checkMail(@RequestBody Map<String, String> entity) {
        return commonService.checkMail(entity);
    }

    @PostMapping(value = "/sign-up")
    public ResponseEntity<String> signUp(@Valid @RequestBody SignUpRequest signUpRequest) {
        return commonService.signUp(signUpRequest);
    }

    @PostMapping("/log-in")
    public ResponseEntity<Map<String, String>> logIn(@Valid @RequestBody LogInRequest logInRequest) {
        return commonService.logIn(logInRequest);
    }

    @PostMapping("/log-out")
    public ResponseEntity<String> logOut(@RequestBody Map<String, String> entity) {
        return commonService.logOut(entity);
    }

    @PostMapping("/get-question")
    public ResponseEntity<String> getQuestion(@RequestBody Map<String, String> entity) {
        return commonService.getQuestion(entity);
    }

    @PostMapping("/update-password")
    public ResponseEntity<String> updatePassword(@Valid @RequestBody UpdatePasswordRequest updatePasswordRequest) {
        return commonService.updatePassword(updatePasswordRequest);
    }

    @GetMapping("/show-movies")
    public ResponseEntity<List<MovieInformation>> showMovies() {
        return commonService.showMovies();
    }

    @GetMapping("/show-series")
    public ResponseEntity<List<SeriesInformation>> showSeries() {
        return commonService.showSeries();
    }

    @PostMapping("/show-by-search")
    public ResponseEntity<Map<String, Object>> showBySearch(@RequestBody Map<String, String> entity) {

        return commonService.showBySearch(entity);

    }

    // New API

    @PostMapping("/show-content")
    public ResponseEntity<Map<String, Object>> showContent(@RequestBody Map<String, String> entity) {

        return commonService.showContent(entity);
    }

    @PostMapping("/get-movie")
    public ResponseEntity<MovieInformation> getMovie(@RequestBody Map<String, Integer> entity) {

        return commonService.getMovie(entity);
    }

    @PostMapping("/get-series")
    public ResponseEntity<SeriesInformation> getSeries(@RequestBody Map<String, Integer> entity) {

        return commonService.getSeries(entity);
    }

    @PostMapping("/get-seasons")
    public ResponseEntity<List<SeasonInformation>> getSeasons(@RequestBody Map<String, Integer> entity) {

        return commonService.getSeasons(entity);
    }

    @PostMapping("/get-episodes")
    public ResponseEntity<List<EpisodeInformation>> getEpisodes(@RequestBody Map<String, Integer> entity) {

        return commonService.getEpisodes(entity);
    }

}
